import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr, 'fr');

import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatIconModule } from '@angular/material';
import { MatTableModule, MatNativeDateModule, MatInputModule, MatButtonModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { CalendarMainViewComponent } from './calendar-main-view/calendar-main-view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarEventComponent } from './calendar-event/calendar-event.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarMainViewComponent,
    CalendarEventComponent
  ],
  imports: [
    BrowserModule,
    DragDropModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatIconModule,
    MatTableModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule
  ],
  providers: [{
    provide: MatDialogRef,
    useValue: 'fr-FR'
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
