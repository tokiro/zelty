import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-calendar-event',
  templateUrl: './calendar-event.component.html',
  styleUrls: ['./calendar-event.component.css']
})
export class CalendarEventComponent implements OnInit {
  @Output() createEvent = new EventEmitter<string>();
  event: string;

  constructor() { }

  ngOnInit() {
  }

  addEventOnParent(event) {
    this.createEvent.emit(event);
    this.event = '';
  }
}
