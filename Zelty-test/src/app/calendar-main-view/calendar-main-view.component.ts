import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem, CdkDragEnter } from '@angular/cdk/drag-drop';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import './date-prototype.ts';

Date.prototype.addDays = function(days) {
  const date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

export interface HourAndDay {
  hour: number;
  monday: Array<string>;
  tuesday: Array<string>;
  wednesday: Array<string>;
  thursday: Array<string>;
  friday: Array<string>;
  saturday: Array<string>;
  sunday: Array<string>;
}

const HOUR_AND_DAY: HourAndDay[] = [];
const WEEK: Array<string> = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

const MIN_HOUR_TO_SHOW = 0;
const MAX_HOUR_TO_SHOW = 23;

@Component({
  selector: 'app-calendar-main-view',
  templateUrl: './calendar-main-view.component.html',
  styleUrls: ['./calendar-main-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class CalendarMainViewComponent implements OnInit {
  eventShow = false;
  eventColumList: string[] = ['DisneyLand', 'Rendez vous chez Zelty', 'Signature du contrat'];
  elements = {};
  displayedColumns: string[] = ['hour', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
  monthNames: string[] = ['janvier', 'fevrier', 'mars', 'avril', 'may', 'juin',
    'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  dataSource: HourAndDay[] = HOUR_AND_DAY;
  firstDayNb: Date;
  month: string;

  constructor() {
  }

  newElementObject() {
    return ({
      sunday: [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []],
      monday: [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []],
      tuesday: [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []],
      wednesday: [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []],
      thursday: [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []],
      friday: [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []],
      saturday: [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
    });
  }
  addEvent(event: string) {
    if (event !== null) {
      this.eventColumList = [...this.eventColumList, event];
    }
    event = '';
    this.eventShow = false;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    sessionStorage.setItem(this.firstDayNb.getDate() + '/' + (this.firstDayNb.getMonth() + 1), JSON.stringify(this.elements));
  }

  addData(data: string, hour: number, day: string) {
    this.elements[day][hour] = [...this.elements[day][hour], data];
    sessionStorage.setItem(this.firstDayNb.getDate() + '/' + (this.firstDayNb.getMonth() + 1), JSON.stringify(this.elements));
  }

  storageElements() {
    const sessionDb = sessionStorage.getItem(this.firstDayNb.getDate() + '/' + (this.firstDayNb.getMonth() + 1));
    if (sessionDb) {
      this.elements = JSON.parse(sessionDb);
    } else {
      this.elements = this.newElementObject();
      sessionStorage.setItem(this.firstDayNb.getDate() + '/' + (this.firstDayNb.getMonth() + 1), JSON.stringify(this.elements));
    }
    return sessionDb;
  }

  addWeek() {
    this.firstDayNb.setDate(this.firstDayNb.getDate() + 7);
    this.storageElements();
  }

  subWeek() {
    this.firstDayNb.setDate(this.firstDayNb.getDate() - 7);
    this.storageElements();
  }

  getMonday(d: Date) {
    d = new Date(d);
    const day = d.getDay();
    const diff = d.getDate() - day + (day === 0 ? -6 : 1);
    return new Date(d.setDate(diff));
  }

  ngOnInit() {
    for (let i: number = MIN_HOUR_TO_SHOW; i <= MAX_HOUR_TO_SHOW; i++) {
      this.dataSource.push({ hour: i, monday: [], tuesday: [], wednesday: [], thursday: [], friday: [], saturday: [], sunday: [] });
    }

    const today = new Date();
    const day = WEEK[today.getDay()];
    this.firstDayNb = this.getMonday(today);
    this.month = this.monthNames[today.getMonth()];
    this.elements = this.newElementObject();
    if (!this.storageElements()) {
      this.addData('Maintenant', today.getHours(), day);
    }
  }
}
