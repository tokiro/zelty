import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarMainViewComponent } from './calendar-main-view.component';

describe('CalendarMainViewComponent', () => {
  let component: CalendarMainViewComponent;
  let fixture: ComponentFixture<CalendarMainViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarMainViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarMainViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
