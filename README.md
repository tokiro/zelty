# Calendar project for Zelty

Project made by Florent LAIR for [Zelty](https://zelty.fr/).
Project made on Angular 8 with material design.

You can find some of other project I made
* Payment page for Wynd (UX/ UI creation + integrating solo). [Link](https://drive.google.com/file/d/1Je-iFQaA_pelAFXXme3_p__98Gzhsl06/view?usp=sharing)
* Backoffice page for Swiper (UX/ UI creation + integrating solo). [Link](https://drive.google.com/drive/folders/16YFRNAjESOUAc9SZIjxNTNl9Vx7XJeWb?usp=sharing)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install the project.

```bash
cd Zelty-test
npm install
ng serve --open
```

## Feature

* View of the week
* New event creation
* Drag and drop the event in the calendar
* No linter error

## To add

You can find a little view of what I wanted [here](https://docs.google.com/presentation/d/11KqMBFUnHbCuZMYng3iQOoa21_VSp0nH8-f92vt2uuU/edit?usp=sharing).

* Change the view (Month / Week / Day)
* Choose and change the week with selector or datepicker
* Store the data on a server DB
* Better event creation
* Responsive creation event
